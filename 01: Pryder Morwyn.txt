{9} Llapiai’r tonnau’n dawel ar odre gwal y cei.
Ehedai’r gwylanod o gwmpas gan grio’n aflafar a cherdded wedyn yn rhodresgar ar y cei ei hun.
Eisteddai Winni ar y wal yn synfyfyrio gan afael yn dynn yn handlen y goits bach a ddaliai Robert, mab ei meistr a’i meistres.
Chwaraeai ef gyda’i gi clwt gan roi bloedd yn awr ac yn y man a lluchio’r ci allan.
Fel y gwnâi hynny codai Winni’r ci a’i roi yn ôl iddo.
Yr oedd pob man yn ddistaw, heb neb yn cerdded ar y cei ar yr awr yma rhwng cinio a the.

Ar hynny, fe ymddangosodd llanc tua deunaw neu ugain oed ac eistedd yn ddigwilydd ar y wal yn ymyl Winni.
Cododd hithau ar amrantiad a chychwyn i ffwrdd efo’r goits.

‘Na,’ meddai ef, ‘chi ddim yn mynd i ffwrdd felna.’

‘Ydw,’ meddai hithau, ‘mae meistres yn fy nisgwyl.’

Gwisgai’r llanc goler stic-up ddwbl ac esgidiau bach, pethau a ddiystyrai Winni yn ei dull gwledig o feddwl.
Esgidiau uchel a wisgai hogiau’r wlad a choler sengl.

{10} ‘Fi eisio siarad efo chi,’ meddai ef, ‘eisio i chi ddwad am dro efo mi.’

‘Ewch adre at ych mam i gael ditan, y llo glyb.’

‘Bedi llo? Cyw buwch ia?’

‘’Rydw i’n dweud wrthoch chi eto, ewch adre at ych mam, a dysgwch siarad yn iawn.’

Ar hynny, pwy a ddaeth heibio ond ei meistr, a dechreuodd y babi weiddi’n afieithus, “Dad, Dad.”

‘Well i chi fynd adre, Winni,’ meddai.

Ac felly y bu.
Cochodd Winni a daeth poen i’w brest.
Meddyliodd peth mor chwithig oedd i’w meistr ddyfod heibio ar yr union funud pan wrthwynebai hi gynigion y llanc yma yn ei dull a’i hiaith gryfaf~— ond un.
Mae’n debyg mai wedi ei regi y byddai petai wedi dal ymlaen i siarad.
Gwyddai y gelwid hi i gyfrif am hyn gan ei meistres.
A dyna fu.

‘Winni, mi welodd Mr.\ Hughes chi yn siarad efo rhyw fachgen ar y cei gynnau.’

‘Do, meistres, ond ’doedd gen i ddim o’r help.’

‘’Rydach chi’n rhy ifanc i siarad efo hogiau.’

‘’Roeddwn i’n eistedd ar wal y cei ac yn gafael yn dynn yn handlen y goits bach pan ddoth yr hogyn yma heibio ac eistedd wrth fy ymyl.’

Ar hyn, daeth y meistr yno.

‘Ia, Winni,’ meddai ef, ‘beth oedd arno fo eisiau?’

‘Eisiau imi fynd am dro efo fo.’

‘A beth ddywedsoch chi?’

‘Mi tafodais o yn iawn a dweud wrtho am fynd adre at ’i fam.
Mi wnes bob dim ond ei regi.’

Gwenodd y meistr.
Aeth Mrs.\ Hughes ymlaen.
‘Oeddach chi wedi’i weld o o’r blaen?
Mae’n edrach {11} yn beth rhyfedd iawn i mi fod o’n gofyn hynna os \sic{dynna}’r tro cynta i chi ei weld o.’

‘Ia’n wir, Mrs.\ Hughes.’

Torrodd y meistr i mewn.

‘Mi alla i’n hawdd gredu hynny.
’Rydw i’n nabod ’i deulu o.
Rêl ciaridýms.’

Aeth y feistres ymlaen.

‘Ydach chi’n gweld, Winni, mi alla i’n hawdd ych gyrru chi adre oddi yma.’

Ar hyn, torrodd Winni i grio.

‘Ond wir, Mrs.\ Hughes, ’doedd gen i ddim o’r help\sic{,}
A plis, peidiwch â ngyrru fi adre.
Mi rydd fy nhad gweir i mi, ac ella fy nhroi dros y drws.
Mi ’rydw i mor hapus yma efo Robert bach.

Meddai’r meistr

‘Peidiwch â bod yn rhy gas wrthi, Mary.
’Rydach chi’n gwybod bod Winni’n forwyn dda, yn well o lawer na’r rhai sydd wedi bod yma.
Colled fawr fyddai ei cholli.’

‘Ia, ’rydw i’n cydnabod hynny.
Mi gawn weld.
Ewch i ddechrau hwylio te, Winni.’

Ni chysgodd Winni fawr y noson honno.
Fe’i gwelai ei hun yn cael ei throi o’i lle, ac oherwydd hynny yn methu cael lle arall.
Ei thad yn rhoi cweir iddi ac yn ei throi allan, a hithau heb unlle i fynd.
Âi trwy’r pethau yma o hyd ac o hyd yn ei gwely a dyfod i atalfa a’r boen yn cynyddu.

Trannoeth, dydd Sadwrn, yr oedd yn brynhawn rhydd iddi, ac fe âi adref, ond dim ond o achos Sionyn bach.
Ni feiddiai ddweud wrth ei thad a Lisi Jên am ei helynt.
Byddai’n rhaid iddi ragrithio a chymryd arni fod yn siriol, peth yr oedd yn gyfarwydd ag ef er pan ddaethai’r ail wraig i’w chartref.

{12} Penderfynodd godi’n fore a golchi dillad Robert cyn golchi llestri brecwast.
Fel arfer rhoes ei meistr swllt iddi i dalu am ei brêc.

Yr oedd Lisi Jên wedi rhoi slemp o llnau, ei thad wedi newid ac ar fin cychwyn i’r dref.
Rhoes Sionyn groeso mawr iddi a mynnodd gael eistedd ar ei glin i gael te.
Yr oedd y te yn dda, ei llysfam wedi gwneud teisen fwyar duon.

Ond synhwyrai Winni fod tyndra yn y tŷ.
Nid oedd ei thad a’i wraig yn siarad â’i gilydd.
Reit siŵr eu bod wedi ffraeo.
Fe gâi ei llysfam dipyn o flas y tafod a gawsai mam Winni.
Pan gychwynnodd i ffwrdd yr oedd Sionyn yn beichio crio ac aeth hithau i grio.
Nid oedd y plentyn yn cael llawer o hapusrwydd.
Yr oedd ei ddillad yn ddi-raen ac nid oedd ganddo lawer o deganau.
Yr oedd trueni ganddi drosto.
Pan ddeuai hi i gael rhagor o gyflog fe brynai deganau iddo a dillad newydd.
Ar hyn o bryd yr oedd arni ddigon o eisiau dillad newydd iddi hi ei hun.
Yr oedd ei meistr wedi dweud na châi ei thad afael yn ei chyflog.
Fe rôi ei chyflog i’w meistres a gofyn iddi hi ei wario ar ddillad i Winni.

Penderfynodd Winni fynd at y mynydd am dro.
Daeth arni ryw fath o hiraeth am yr amser y byddai hi a Begw yn ei groesi, er nad oedd hwnnw yn amser braf.
Ond nid oedd y boen hon ar ei meddwl y pryd hwnnw.

Yr oedd y mynydd yn gochddu gan liwiau’r Hydref, y ffrwd yn llifo’n dawel, undonog ac yn glir ei dŵr.
Yr oedd yr haul ar fin machlud fel pelen goch ac yn taflu ei wrid ar y môr.
Gymaint y byddai’n mwynhau hyn i gyd petai hi heb boen meddwl.
Eisteddodd ar lan yr aber fach a dechreuodd {13} grio eto.
Daeth hiraeth arni am ei mam.
Pe buasai hi’n fyw, fe gawsai ddweud ei holl drwbwl wrthi, ac fe fuasai ei mam yn dweud, ‘Hitia befo, mi cadwa i di gartre.
Mae yma ddigon o waith iti, ac mi gei fynd i’r ysgol nos ac i gyfarfodydd y capel.’

Daeth dyn o’r mynydd, hen gymydog, sachaid o boethwal ar ei gefn a chi defaid wrth ei ochr.
Eisteddodd ar y dorlan a’r ci yntau.

‘Hylo, Winni, sut mae hi ers talwm?’

‘Da iawn diolch, Huw Tomos, a chitha?’

‘Iawn diolch.
Sut mae hi’n mynd tua’r dre’ yna?’

‘Iawn.
’Rydw i wrth fy modd, wedi cael lle da iawn.
Mae yno hogyn bach annwyl, tua’r dwyflwydd yma.’

Gwrandawai’r ci fel petai’n deall y cwbl, a sylwodd Winni ar ei lygaid ffeind, mor wahanol i lygaid ei thad a’i llysfam y prynhawn yma ac mor debyg i lygaid Sionyn.

Penderfynodd fynd i weld Elin Gruffydd, mam Begw, a dweud y cwbl wrthi.
Ar y funud diflannodd ei phoen a daeth rhyw dawelwch i’w chalon.
Dechreuodd y niwl lithro dros y mynydd, a chyrraedd a chreu diferion fel gwlith ar ei gwallt.
Cododd hithau a ffarwelio â’r dyn a’r ci.

‘Dowch i mewn Winni.
Beth sy’n bod?
’Rydach chi wedi bod yn crio?’

‘Do, mi ’rydw i mewn helynt, a mi es at y mynydd.
Mi ddoth arna i hiraeth ar ôl mam o achos yr helynt.’

‘Beth sy’n bod?
’Dydach chi ’rioed wedi anufuddhau i’ch meistres?’

‘Naddo, ddim o gwbl.’

‘Da clywed hynny.’

{14} Rhoes Winni’r hanes a Begw’n gwrando, ei cheg yn agored a’i llygaid yn rhythu.
Pan ddaeth Winni at ei thafodiad wrth y llanc, y ‘llo glyb’ ag ati, chwarddodd y ddwy arall.

‘Gellwch chi ei weld yn ddigri, ond ’doedd o ddim yn ddigri ar y pryd.
Mi fedrais ddal fy nhafod rhag rhegi, a da hynny, achos pwy ddoth heibio ond meistr.’

‘Mi fuasai’n rhaid i ffawd i anfon heibio y funud honno,’ ebe Elin Gruffydd.

‘Ond ’doedd gen i ddim o’r help.
Dyna oeddwn i’n drio’i ddweud wrthyn ’nhw yn y tŷ wedyn.’

‘Oedden ’nhw’n gas?’

‘O nac oedden’, ond mi roes meistres ryw hỳm y buasai hi’n medru rhoi’r wib i mi.’

‘Am hynny bach?’

‘Dyna oeddwn innau’n feddwl, a dyna sydd wedi fy mhoeni fi er ddoe.’

‘Ddaru chi ddweud wrth ych tad?’

‘O bobl, naddo.
Mi roesai gweir imi, a nhroi fi dros y drws, ac i ble’r awn i wedyn?’

‘Yma,’ meddai Elin Gruffydd a Begw gyda’i gilydd.

‘O diolch, dyna ysgafnhad.’

‘Dydw i ddim yn meddwl y try ych meistres chi oddi yno os ydach chi yn ’i phlesio, Winni.’

‘Wel, mi ddwedodd Meistr mod i’n well morwyn na’r rhai fu yno o’r blaen, ac mi roth swllt imi dalu am y frêc.’

‘Dyna fo.
Da iawn.
Gewch chi weld y bydd popeth yn iawn.
Anodd iawn cael genod ifanc da iawn rwan am gyflog bach, yn enwedig lle mae plant neu {15} blentyn.
Codwch ych calon, Winni.
Mi gymerwn ni damaid o fwyd.’

A chafodd y tair de efo wy wedi’i ferwi.

Yr oedd Winni yn llawer hapusach yn y frêc wrth fynd yn ôl i’r dref.
Ni byddai ar y clwt a byddai Elin Gruffydd yn siŵr o ffeindio lle arall iddi.

Pan gyrhaeddodd ei lle rhyfeddodd weld goleuadau llachar ymhobman.
Daeth ei meistres i gyfarfod â hi yn y lobi a Robert yn ei llaw.
Dylai’r bachgen fod yn ei wely.

‘Mae Robert wedi gwrthod mynd i’w wely nes i chi ddwad yn ôl Winni.
A diolch i chi am olchi ’i ddillad.
Mi smwddiais i nhw y pnawn yma.’

‘Mi gawn ni i gyd swper yn y selar heno er mwyn cadw’r topiau yn lân at yfory.
Mi awn ni i gyd i’r capel bore fory, a mi gaiff Robert ddwad efo ni.’

Gwenodd yn groesawgar.

‘Ac o hyn allan mi awn ni i gyd fel yna i’r capel bob bore Sul.’
