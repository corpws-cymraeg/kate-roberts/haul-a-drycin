{24} Eisteddai Deina prys yn y gadair freichiau ar ei haelwyd o flaen tanllwyth o dan mawn gan synfyfyrio ac edrych ar ei gwaith.
Yr oedd wedi gorffen am y diwrnod.
Yr oedd wyneb y bwrdd cwpwrdd wedi ei sgwrio’n lân efo graean gwyn, a’r un graean wedi ei sgeinio ar y llawr pridd.
Yr oedd y dresel, y cwpwrdd pres a’r cloc wedi eu rhwbio’n loyw.
Safai’r rhai hyn ar lwyfan lechen a lluniau pysgod wedi eu cerfio ar y tu blaen iddi.
Ymfalchïai Deina yn ei dodrefn derw; yr oedd ganddi gist derw a bwrdd yn y siamber hefyd, y cwbl wedi dyfod o’r hen gartref ar waelod y plwy, Pant y Llyn.
Yr oedd Harri, ei gŵr, wedi medru cadw digon o arian i brynu cadeiriau, byrddau a phethau eraill i’r gegin.
Safai troell fach ar y llawr, a chyda hon yr oedd hi wedi nyddu llenni o lin i’w rhoi ar y ffenestri.

Ugain oed oedd hi ac wedi priodi ers pythefnos. Yr oedd rhyw bedair awr eto nes deuai ei gŵr o’r chwarel, a hithau heb ddim i’w ddarllen.
Nid oedd fawr o lyfrau yn ei hen gartref, \worktitle{Y Bardd Cwsg} a \worktitle{Llyfr y Tri Aderyn}.
Ond yr oedd ganddi hosan ar {25} y gweill a chai ei meddyliau redeg efo’r gwau.
Âi ei meddwl yn ôl at y dydd y daeth Harri yn was Bach i Band y Llyn.
Carodd ef y munud cyntaf y gwelodd ef, pan oedd hi yn un ar bymtheg ac yntau yn ddeunau.
Yr oed yn fachgen cydnerth, gyda llygaid gwinau cynnes, gwallt cringoch, ffurf pen hardd, dannedd da, a chroen ac ôl y tywydd arno.
Âi hi allan i’r buarth bob cyfle a gâi er mwyn ei weld a thynnu sgwrs ag ef.
Ond âi heibio iddi’n swil â’i ben i lawr, nes y mentrodd hi ryw ddiwrnod dorri gair ag ef, a gofyn yn ddigywilydd a oedd yn medru darllen ac ysgrifennu.
Wedi iddo ateb nad oedd ac y rhoddai’r byd am gael dysgu, dywedodd ei bod hi am ofyn i’w thad a gâi ddyfod i’r tŷ ac iddi hi ei ddysgu.
(Yr oed hi a’i chwiorydd wedi cael tipyn o ysgol gyda hen ferch yn y dref.)
Cafodd ganiatâd, a hithau yn dysgu iddo ddarllen â’r Testament o’i flaen.
O’i flaen hefyd yr oedd darnau o hen bapurau a chwilsyn gŵydd iddo ddysgu gwneud y llythrennau.
Byddai hi yn ei nefoedd, dim ond cael edrych arno, er bod oglau stabal arno.
Byddai arni eisiau gafael yn ei law, a dweud wrtho ei bod yn ei hoffi, ond byddai ei chwiorydd, Grasi ac Elisabeth, yn gwilio pob symudiad o’i heiddo, a’i thad hefyd weithiau.

Yn y llofft stabal y cysgai Harri, ac ni byddai siawns yn y nos i’w weld ar ôl i’r gwersi orffen.
Byddai arni weithiau awydd mynd i fyny i’r lloft stabal, ond ni chlywai ddiwedd ar edliw ei chwiorydd petai’n mynd.
Gwelai gip arno yn ystod y dydd adeg y prydau bwyd pan ddeuai’r gweision i mewn i’r gegin.
Yr oedd wedi sylwi ar yr adegau yma fel y byddai Leusa’r forwyn yn hofran o gwmpas Harri {26} yn tendio arno, a deuai cenfigen i’w chalon.
Penderfynodd y byddai’n rhaid iddi ddweud wrtho ei bod yn ei garu; a ryw ddiwrnod pan gyfarfu ag ef ar y buarth dywedodd hynny wrtho.
Rhoes ef ei ben i lawr a dywedodd yn floesg ei fod yntau yn ei charu hi.
Yr oedd hi yn ei nefoedd byth wedyn, er na châi ond ambell sgwrs fer ag ef ar y buarth.

Daeth ei chwiorydd i sylwi ar y siarad yma, ac aethant i ben Deina a dweud wrthi nad oedd i fod i lolian efo’r gwas, nad oedd yn beth iawn i ferch ffarm fel Pant y Llyn briodi efo’r gwas.
Dywedasai hithau wrthynt yn bur blaen na phriodai neb arall.
Yr oeddynt yn hŷn o dipyn na hi, a dywedodd Deina wrthynt mai hen ferched a fyddent ar hyd eu hoes os dalient i ddisgwyl am rywun gwell.
Ond y peth mawr oedd ei bod yn caru Harri, ni fedrai ei chwiorydd byth ei throi oddi wrth hynny.

Ond digwyddodd rhywbeth a wnaeth i’r ddau weld ei gilydd yn anamlach.
Daeth sôn bod nifer o ddynion yn mynd i agor chwarel ar ochr y mynydd, dair milltir i ffwrdd, a dechreuodd Harri gymryd diddordeb ynddo a mynd ar nos Sadwrn i weld rhai o’r bobl hyn.
Y diwedd fu iddo adael Pant y Llyn ac ymuno â’r dynion yma.
Yr oedd ei thad wedi gwneud popeth i geisio ganddo aros, rhoi mwy o gyflog iddo a rhoi gwell lle iddo gysgu, gan ei fod yn weithiwr mor dda.
Ond yr oedd yn well gan Harri gael mwy o ryddid a mwy o fenter.
Colled fawr oedd ei golli.

Yr oedd yn chwith iawn gan Deina yn awr beidio â chael gweld ei chariad ar y buarth, a bu’n rhaid iddi ddyfeisio rhyw ffordd o gael ei weld. Yr oedd wedi gyrru ati drwy un o’r gweision ym mha le yr {27} oedd yn lletya, a phenderfynodd hithau fynd yno ar brynhawn Sadwrn.
Fe’i gwelodd cyn iddo gychwyn i’r dref ar ei draed.
Cafodd ei gwmpeini bron hyd at ei chartref, a’r prynhawn hwnnw y medrodd ef ddweud wrthi faint yr oedd yn ei charu, a dywedodd y caent briodi wedi iddo godi’r bwthyn yr oeddynt i fyw ynddo.
Yr oeddynt yn ennill arian reit ddel wrth godi llechi oedd bron ar wyneb y tir, eu gwerthu i ddyn yng Nghaer Saint a rhannu’r arian rhwng y chwech ohonynt.
Cafodd dafod iawn gan ei chwiorydd wedi mynd adref, ond ni lwyddasant i’w rhwystro rhag mynd i weld Harri fel hyn ar brynhawniau Sadwrn.
Yr oedd ei chartref yn lle annifyr erbyn hyn, y fam wedi marw, y chwiorydd yn gas , ei thad yn ddyn trist oherwydd colli ei wraig, ac er nad oedd yn gwrthwynebu carwriaeth Deina ar lafar, nid oedd yn dweud dim i godi ei chalon ychwaith.
Edrychai’n brudd arni.
Rhwng popeth, nid oedd bleser i Deina yn ei chartref, ond yr oedd bodlonrwydd yng ngwaelod ei chalon wrth feddwl am y Sadwrn o hyd.
Ond O! yr oedd wythnos yn amser hir.

Aeth pethau ymlaen fel hyn am ddwy flynedd, ac o’r diwedd dywedodd Deina wrth ei thad a’i chwiorydd ei bod ym mynd i briodi.
Ni ddywedodd Grasi fawr ddim, ond dywedodd Elisabeth, ‘Rwyt ti wedi mynnu ei gael, mi fydd yn rhaid iti fyw efo fo ’rŵan.’
Daeth dagrau i lygaid ei thad pan ddywedodd. ‘Mi f’asai yn well gen i dy weld yn priodi mab ffarm, ond dy hapusrwydd di sy’n bwysig, a gwbeithio y byddwch chi eich dau yn hapus. Mae Harri yn weithiwr da.’

Priodasant yn yr eglwys ar fore tywyll ym mis {28} Rhagfyr.
Cytunodd Grasi i fod yn forwyn briodas a’r hwsmon i fod yn was, a safai ei thad yn ymyl.
Daeth Elisabeth i’r gwasanaeth.
Nid oedd gan Harri deulu, plentyn y wyrcws oedd o.

Werth eistedd wrth y tân yn y fan yma prynhawn yma cofiai Deina am y pethau bach yn y gwasanaeth~— yr eglwys yn oer a thywyll, y gwêr brwynog yn disgyn hyd ochr ar draws yr alor, a’r offeiriad yn mynd trwy’r gwasanaeth mewn llais di-liw, undonog.
Gwisgai hi sgert sidan ddu gwmpasog a rhesi gwyrth ar hyd-ddi, bods melfed du a siôl bersli ei mam.
Yr oedd siwt o frethyn cartref am Harri wedi i’r teiliwr ddyfod i Bant y Llyn i’w gwneud trwy garedigrwydd ei thad.
Er mor ddigalon ac oer yr edrychai pob dim yn yr eglwys, yr eodd calon Deina yn llosgi gan hapusrwydd am fod ei breuddwyd wedi dyfod i ben.
Cawsant frecwast ym Mhant y Llyn o gig hallt wedi ei ferwi, moron a thatws, a phwdin reis efo digon o wyau ynddo.

Aeth y ddau i’w tŷ newydd y prynhawn hwnnw.

Mynd dros y pethau yna yr oedd yn ei meddwl ar hyn o bryd, a gwenu wrth gofio am yr hapusrwydd a ddygasai megis oddi wrth ei theulu a’i gael yn Harri.

Cododd yn sydyn ac aeth i’r beudy i weld y llo bach, rhodd ei thad.
Teimlai nad oedd yn cael chwarae teg wrth fod yma ar ei ben ei hun heb ei fam.
Ond yr oedd Harri wedi clywed hanes heffer dda yng ngwaelod y plwy, ac yr oedd am fynd yno y Sadwrn dilynol i’w gweld ac efallai ei phrynu.
Pranciodd y llo pan welodd hi ac ysgwyd ei ben.
Pratiodd hithau ef a rhoi ei llaw iddo ei llyfu.

Dotiai hithau arno a chochodd wrth feddwl ei bod yn ymddwyn fel plentyn a hithau’n wraig erbyn hyn.
Fel yna y byddai gyda phob llo bach pan oedd yn blentyn.

Yr oedd niwl yn ymdorchi am y tŷ a thrwyddo gwelai ddynes yn dyfod trwy’r llidiart, basged ar ei braich a phiser yn ei llaw.

‘’Dydach chi ddim yn fy nabod i?’ meddai wrth gamu i’r tŷ. ‘Marged Huws, Twll Mawn, ydw i, ac mi feddyliais y dylwn i ddwad i edrych amdanoch chi a chithau yn ddiarth yn y lle yma.
Clywais nad ydach chi ddim wedi cael buwch eto, ac mi ddois i â phrintan o fenyn, torth haidd a llaeth enwyn i chi.’

‘Wel diolch yn fawr i chi, ’rydw i’n falch iawn o’ch gweld chi.
’Does yma neb wedi twllu’r tŷ yma heddiw ar ôl i Harri fynd i’r chwarel.
Mynd i weld y llo ddaru i mi ’rŵan am fy mod i wedi diflasu bod yn y tŷ heb ddim i’w wneud.’

‘Mi fydd, mi a’ i i’r lle y bydd ar Harri eisio mynd, ac yn ôl ’i wynt o, i’r capel y bydd o’n mynd.’ exception

‘Maen’ nhw am drio cael John Jones, Talysarn, yma i agor y capel.
Yr ydym ni wedi mynd fel paganiaid yn y topiau yma ’rŵan; mae pawb yn gweld ’i bod hi’n rhy bell i fynd i’r eglwys.’

‘Fyddech chi ddim llawer elwach wedi mynd i’r eglwyss,’ meddai Deina, ‘mae’r person yna’n anobeithiol, a mwy na hanner y gwasanaeth yn Saesneg a neb yn ’i ddeall.
’Does fawr neb sy’n byw o gwmpas yn mynd yno ’rŵan.
Mi ’rydw i am wneud {30} paned o de inni; mi gefais dipyn gan fy chwaer a hithau wedi cael peth gan ryw ffrind cefno o Sir Fôn.’

‘Mae o’n ddrud iawn ac yn beth amheuthun iawn i ni bobol y chwareli.’

Cawsant frechdan haidd a bara ceirch efo’r te.

‘Ardderchog,’ meddai Marged Huws, ‘te ffit i’r frenhines.
Mi wnewch wraig iawn i Harri, ac mae yntau yn fachgen gweithgar.
’Dwn i ddim sut y bydd hi ’rŵan wedi’r newid yn y chwarel yna.
Ydach chi’n gweld, o’r blaen ’roedd y dynion yn gweithio ar y cŷd ac yn rhannu’r elw.
Mi gawson waith caled i gael gwared o’r grug a thyrchu at y llechen; ac mi ’roedd yn rhaid iddyn’ nhw gael troliau a cheffylau i fynd â’r llechi i lawr i’r dre a’u gwerthu nhw i ryw ddyn yn fanno a hwnnw yn ’i gyrru nhw i rywle arall.
Ond er pan mae’r dyn newydd yma wedi prynu’r chwarel, cyflog maen’ nhw yn ’i gael.’

‘Wel ia, ac mae Harri yn rhoi’r rhan fwya ohoni i mi.
Mae o’n cwyno fod ar y dyn newydd yma eisio gwneud elw, ac yn ’i gyrru nhw.
Mae o’n mynd i lawr ac yn agor lefelydd yn y graig.’

‘Ydyn, mae o’n lle digon peryg erbyn hyn.
Ond i beth y siarada i fel hyn efo rhywun ifanc?
Mi gliria’r niwl yma, ac mi ddaw y gwanwyn, a dyna olygfa gewch chi, o’r môr a Sir Fôn.’

Yr oedd Deina’n mwynhau’r sgwrs ac yn meynhau edrych ar Marged Huws.
Yr oedd ganddi wyneb rhadlon, a dim ond un dant yn ei phen.
Dynes tua’r deugain oed oedd hi, ond yn edrych yn llawer hŷn.
Gwisgai glocsiau a sannau gwynion, pais stwff a becwn, barclod cros bar glas a gwyn, siôl drosti, a {31} chap gwyn wedi ei gwicio a het ddu fechan am ei phen.

Teimlai Deina’n hapus wedi iddi fod, ac yn fwy o wraig wedi iddi gael gwneud te i gymdoges.
Gobeithiai y deuai rhai eraill i edrych amdani.
Yr oedd yn falch o gael y llaeth a’r menyn.
Aeth ati i wneud llith i’r efo’r llaeth; fe sbariai hynny rywbeth i Harri wedi iddo ddyfod adre.
Yna dechreuodd hwylio swper chwarel; rhoes sosban a’i llond o datws trwy’u crwyn ar y tân mawn a hwnnw’n gwreichioni wrth gael ei symud.
Toc, byddai Harri gartref, ac mor braf fyddai cael anghofio’r hyn a ddigwyddasai ym Mhant y Llyn cyn ei phriodas, a chael siarad drwy gyda’r nos efo Harri.
